# Stellarator Base Case

## Description
The Stellarator Base Case (SBC) is meant as a starting point for codes written for 3D magnetic confinement equilibria (most likely stellarators) to be tested in a true 3D equilibrium, which contains all important features (such as varying geometry for different field lines on a given flux surface) while keeping some of the characterists of current real equilibria, such as Wendelstein 7-X with its plethora of trapping wells and intricate magnetic field structure, or the HSX equilibrium with its very low global shear, managable.

![magnetic field strenght on the outermost flux surface of the stellarator base case](https://gitlab.mpcdf.mpg.de/jproll/stellarator-base-case/-/blob/main/Equilibrium/Equilibrium-Images/SBC-plot.png)

The equilibrium at hand is a 2-period quasi-isodynamic device, which originated from near-axis expansion followed by further optimisation for improved quasi-isodynamicity. It has sizeable rotational transform and global shear, and smooth magnetic field strength and metric coefficients, which allow for rather low resolution along the field lines.



## Usage
The equilibrium and provided data are free for all to use, to check whether the way geometry is implemented in your code is correct, and later on for benchmark purposes as well.

## Contributing
If you use the SBC for a new code, we would appreciate it if you (after brief communication with us) would provide us with a contribution to the benchmark, e.g. some files with growth rates for a certain gyrokinetic instability, or similar for other codes, together with some explanatory notes.


## Support
If you have questions, you can contact Josefine via josefine.proll at ipp.mpg.de

## Roadmap
In the long run we would want to create a class of SBCs where, akin to tokamak equilibria being descibred by Miller several parameters can be changed, so that the influence of those parameters on certain physics aspects can be investigated directly. 


## Authors and acknowledgment
The idea of the SBC first originated from discussions between Motoki Nakata and Josefine Proll. The equilibrium exists thanks to Gabriel Plunk, Katia Camacho Mata and Alan Goodman. Christopher Albert was the first non-gyrokinetics person to be willing to try the SBC and showcase its usefulness outside of the gyrokinetics realm.
The idea was further supported by the Coordinated Working Group Meeting.

## License
For open source projects, say how it is licensed.

